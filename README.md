Glass dataset
-------------

Content of each sample directory:

- config.xyz: initial instantaneous configuration in xyz format
- config.xyz.propensity.csv: propensity of motion at different times in csv format, one particle per line
- config.xyz.times.csv: times at which the propensity is calculated

